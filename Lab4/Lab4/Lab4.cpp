﻿// Lab4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <iomanip>
using namespace std;

const double eps = 0.0001;
const int n = 2;
const int var = 23; // номер варианта

// первое приближение
void Initialize(double& x, double& y) {
	if (var == 23) {
		x = 0.0;
		y = 0.8;
	} else {
		x = 0.5;
		y = 0.0;
	}
}

// печать заголовков таблицы
void PrintHeadings1() {
	cout << setw(42) << "оценка" << endl;
	cout << " Itr   |      x     |      y     | погрешности |      F1      |      F2       |  норма якобиана " << endl;
}

void PrintHeadings2() {
	cout << setw(42) << "оценка" << endl;
	cout << " Itr   |      x     |      y     | погрешности |      F1      |      F2       |       FF     | норма якобиана |   альфа " << endl;
}

// печать данных на текущей итерации
void PrintData(int itr, double x, double y, double error, double F1, double F2, double normJ) {
	cout << " " << setw(4) << itr << "  | " << setprecision(8) << x << " | " << y <<
		" | " <<  error << " | " << setprecision(10) << F1 << " | " << F2 << " | " << setprecision(5) <<
		normJ << endl;
}

void PrintData(int itr, double x, double y, double error, double F1, double F2, double FF, double normJ, double alpha) {
	cout << " " << setw(4) << itr << "  | " << setprecision(8) << x << " | " << y <<
		" | " << error << " | " << setprecision(10) << F1 << " | " << F2 << " | " << FF << " | " << setprecision(5) <<
		normJ  << "         | " << alpha << endl;
}

// печать матрицы
void PrintMatrix(double** A) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			cout << fixed << setprecision(5) << setw(8) << A[i][j];
		cout << endl;
	}
	cout << "\n";
}

// нахождение нормы матрицы
double Norm1(double** A) {
	double max = 0;
	for (int i = 0; i < n; i++) {
		double sum = 0;
		for (int j = 0; j < n; j++)
			sum += abs(A[i][j]);
		if (sum > max)
			max = sum;
	}
	return max;
}

// нахождение якобиана
void Jacobian(double** J, double x, double y) {
	if (var == 23) {
		J[0][0] = cos(x) / 2;
		J[0][1] = 0;
		J[1][0] = 0;
		J[1][1] = sin(y - 1);
	} else {
		J[0][0] = cos(x + 1);
		J[0][1] = 0;
		J[1][0] = 0;
		J[1][1] = sin(y) / 2;
	}
}

// 1-я функция
double F1(double x, double y) {
	if (var == 23)
		return sin(x) + 2 * y - 1.6;
	else
		return sin(x + 1) - y - 1;
}

// 2-я функция
double F2(double x, double y) {
	if (var == 23)
		return cos(y - 1) + x - 1;
	else
		return 2 * x + cos(y) - 2;
}

// норма погрешности
double NormError(double x, double y, double xNew, double yNew) {
	double xNew_x = abs(xNew - x);
	double yNew_y = abs(yNew - y);
	return (xNew_x > yNew_y) ? xNew_x : yNew_y;
}

// находим матрицу для исходной системы, элементы которой - производные 1-го порядка по переменным х и у
void InitializeDF(double** DF, double x, double y) {
	if (var == 23) {
		DF[0][0] = cos(x);
		DF[0][1] = 2;
		DF[1][0] = 1;
		DF[1][1] = -sin(y-1);
	} else {
		DF[0][0] = cos(x + 1);
		DF[0][1] = -1;		
		DF[1][0] = 2;
		DF[1][1] = -sin(y);
	}
}

// обратная матрица
void ReverseDF(double** DF) {
	double det = DF[0][0] * DF[1][1] - DF[0][1] * DF[1][0];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			DF[i][j] /= (i + j == 1) ? -det : det;
	swap(DF[0][0], DF[1][1]);
}

// функция, исследуемая на минимум в методе градиентного спуска
double FF(double x, double y) {
	return pow(F1(x, y), 2) + pow(F2(x, y), 2);
}

// находим вектор для функции FF, элементы которого - производные 1-го порядка по переменным х и у
void InitializeDFF(double* DFF, double x, double y) {
	if (var == 23) {
		DFF[0] = 2 * cos(x) * F1(x, y) + 2 * F2(x, y);
		DFF[1] = 4 * F1(x, y) - 2 * sin(y-1) * F2(x, y);
	} else {
		DFF[0] = 2 * cos(x + 1) * F1(x, y) + 4 * F2(x, y);
		DFF[1] = -2 * F1(x, y) - 2 * sin(y) * F2(x, y);
	}
}

// метод простых итераций
void SimpleIterMethod(double x, double y, double** J) {
	double xNew, yNew, q, error;
	int itr = 0;
		
	cout << "Якобиан:" << endl;
	Jacobian(J, x, y);
	PrintMatrix(J);
	cout << "норма J: " << Norm1(J) << endl << endl;
 
	cout << "Метод простых итераций" << endl << endl;
	PrintHeadings1();

	do {
		itr++;
		if (var == 23) {
			xNew = 1 - cos(y - 1);
			yNew = 0.8 - sin(x)/2;
		} else {
			xNew = 1 - cos(y)/2;
			yNew = sin(x + 1) - 1;
		}
		Jacobian(J, xNew, yNew);
		q = Norm1(J);
	    error = NormError(x, y, xNew, yNew)*(1 - q) / q;
		PrintData(itr, xNew, yNew, error, F1(xNew, yNew), F2(xNew, yNew), q);
		x = xNew;
		y = yNew;
	} 
	while (error > eps);

}

// метод Ньютона
void NewtonMethod(double x, double y, double** J) {
	double xNew, yNew, q, error;
	int itr = 0;
	double** DF = new double*[n];
	for (int i = 0; i < n; i++)
		DF[i] = new double[n];
	cout << "Метод Ньютона" << endl;
	PrintHeadings1();
	do {
		itr++;
		InitializeDF(DF, x, y);
		ReverseDF(DF);
		double F1_ = F1(x, y);
		double F2_ = F2(x, y);
		xNew = x - (DF[0][0] * F1_ + DF[0][1] * F2_);
		yNew = y - (DF[1][0] * F1_ + DF[1][1] * F2_);
		Jacobian(J, xNew, yNew);
		q = Norm1(J);
		error = NormError(x, y, xNew, yNew)*(1 - q) / q;
		PrintData(itr, xNew, yNew, error, F1(xNew, yNew), F2(xNew, yNew), q);
		x = xNew;
		y = yNew;
	} while (error > eps);
	for (int i = 0; i < n; i++)
		delete[] DF[i];
	delete[] DF;
}

// метод градиентного спуска
void GradientDescentMethod(double x, double y, double** J) {
	double xNew, yNew, q, error;
	int itr = 0;
	double* DFF = new double[n];
	cout << "Метод градиентного спуска" << endl;
	PrintHeadings2();
	do {
		itr++;
		double alpha = 1, lambda = 0.5;
		InitializeDFF(DFF, x, y);
		while (FF(x - alpha * DFF[0], y - alpha * DFF[1]) >= FF(x, y))
			//cout << "We are here" << endl;
			alpha *= lambda;
		xNew = x - alpha * DFF[0];
		yNew = y - alpha * DFF[1];
		Jacobian(J, xNew, yNew);
		q = Norm1(J);
		error = NormError(x, y, xNew, yNew)*(1 - q) / q;
		PrintData(itr, xNew, yNew, error, F1(xNew, yNew), F2(xNew, yNew), FF(xNew, yNew), q, alpha);
		x = xNew;
		y = yNew;
	} while (error > eps);
	delete[] DFF;
}

int main() {
	setlocale(LC_ALL, "Russian");

	double** J = new double*[n];
	for (int i = 0; i < n; i++)
		J[i] = new double[n];

	double x, y;
	
	Initialize(x, y);
	cout << "Начальное приближение: x = " << x << ", y = " << y << endl;

	SimpleIterMethod(x, y, J);
	cout << endl;

	NewtonMethod(x, y, J);
	cout << endl;

	GradientDescentMethod(x, y, J);
	cout << endl;

	for (int i = 0; i < n; i++)
		delete[] J[i];
	delete[] J;

    return 0;
}
