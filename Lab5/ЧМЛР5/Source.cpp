#include <fstream>
#include <iomanip>
#include <vector>
using namespace  std;

ofstream fout("out.txt");
const int n = 5;
const double a = 1, b = 2;
const double h = (b-a) / n;
const int var = 23; // ����� ��������

// �������� �������
double F(double x)
{
	if (var == 23)
		return pow(3, x) + 2 * x - 3;
	else
		return pow(2, x) - 3 * x + 2;
}

// ����������� �������� �������
double DF(double x)
{
	if (var == 23)
		return log(3)*pow(3, x) + 2;
	else
		return log(2)*pow(2, x) - 3;
}

// ���������� ������� ����������� ���������
vector <vector<double>> FindDividedDifferenceMatrix(vector <double> xVec, vector <double> yVec)
{
	vector <vector<double>> DivDiff(n+1);
	for (int i = 0; i < n+1; i++)
	{
		DivDiff[i].resize(n + 2);
		DivDiff[i][0] = xVec[i];
		DivDiff[i][1] = yVec[i];
	}

	for (int j = 2; j < n+2; j++)
		for (int i = 0; i < n+2 - j; i++)
			DivDiff[i][j] = (DivDiff[i + 1][j - 1] - DivDiff[i][j - 1]) / (xVec[i + j - 1] - xVec[i]);
	return DivDiff;
}

// ������ �������
void PrintVector(vector <double> b)
{
	for (int i = 0; i < 3; i++)
		fout << setprecision(10) << b[i] << endl;
	fout << "\n";
}

// ������ ������� 3�3
void PrintMatrix(vector <vector<double>> A)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			fout << fixed << setprecision(4) << setw(10) << A[i][j];
		fout << endl;
	}
	fout << "\n";
}

// ������ ������� ����������� ���������
void PrintMatrixDivDiff(vector <vector<double>> A)
{
	for (int i = 0; i < n+1; i++)
	{
		for (int j = 0; j < n + 2 - i; j++)
			fout << fixed << setprecision(6) << setw(12) << A[i][j];
		fout << endl;
	}
	fout << "\n";
}

// ����������� ������������ �� ������� �������
double FindErrorForNewton(double x, vector<double> xVec, double M6)
{
	double w = 1;
	for (int i = 0; i < n+1; i++)
		w *= x - xVec[i];
	int fact = 1;
	for (int i = 2; i < n + 2; i++)
		fact *= i;

	return M6 * abs(w) / fact;
}

// ����������� ������������ ���������
double FindErrorForSpline(double M4, double M5)
{
	return (M4 / 384 + M5 * h / 240)*pow(h, 4);
}

// ������������ �� ������� �������
void NewtonFormula(vector <double> xVec, vector <double> xMidVec, vector <double> yVec)
{
	// ������� ����������� ���������
	vector <vector<double>> DivDiff;

	fout << "������� �������\n\n";
	DivDiff=FindDividedDifferenceMatrix(xVec, yVec);
	fout << "������� ����������� ���������:\n";
	PrintMatrixDivDiff(DivDiff);

	double M6 = (var == 21) ? pow(log(2), 6) * pow(2,b) : pow(log(3), 6) * pow(3, b);
	fout << "\nM6 = " << M6 << endl;

	fout << endl << " x  | " << "   f(x)   |" << "   Pn(x)  |" << " �����������              |" << " ������ " << endl;
	for (int k = 0; k < n; k++)
	{
		double f = DivDiff[0][1];
		double w = 1;
		for (int i = 1; i < n+1; i++)
		{
			w *= xMidVec[k] - xVec[i - 1];
			f += DivDiff[0][i + 1] * w;
		}
		double F_ = F(xMidVec[k]);
		fout << setprecision(1) << xMidVec[k] << "  | " << setprecision(6) << F_ << " | " <<
			f << " | " << setprecision(18) << scientific << abs(f - F_) << " | " <<
			FindErrorForNewton(xMidVec[k], xVec, M6) << fixed<< endl;
	}
}

double fi0(double tau)
{
	return (1 + 2 * tau)*pow(1 - tau, 2);
}

double fi1(double tau)
{
	return tau*pow(1 - tau, 2);
}

// ������������ ���������
void SplineInterpolation(vector <double> xVec, vector <double> xMidVec, vector <double> yVec)
{
	fout << "\n������������ ���������� ��������\n";

	vector <double> m(n + 1);

	m[0] = DF(a);
	m[n] = DF(b);

	// ������� ��������� m ������� ��������
	vector <double> alpha(n + 1);
	vector <double> beta(n + 1);

	alpha[1] = 0;
	beta[1] = m[0];

	for (int j = 1; j < n; j++)
	{
		alpha[j + 1] = -1 / (4 + alpha[j]);
		beta[j + 1] = (3 * (yVec[j + 1] - yVec[j - 1]) / h - beta[j]) / (4 + alpha[j]);
	}

	for (int j = n - 1; j > 0; j--)
		m[j] = alpha[j + 1] * m[j + 1] + beta[j + 1];

	// ���������� �������� 4-� � 5-� ����������� ����������� � ����� � = b
	double M4, M5;
	M4 = (var == 21) ? pow(log(2), 4) * pow(2, b) : pow(log(3), 4) * pow(3, b);
	M5 = (var == 21) ? pow(log(2), 5) * pow(2, b) : pow(log(3), 5) * pow(3,b);

	fout << setprecision(7) << "M5 = " << M5 << "\nM4 = " << M4 << endl << endl;

	// ������ ����������� ������������� 1-� �����������
	fout << " x[i] | " << "df/dx(x[i])|" << "     m[i]   |" << " �����������              |" << " ������ " << endl;
	for (int i = 0; i < n+1; i++)
	{
		double DF_xi = DF(xVec[i]);
		fout << setprecision(1) << xVec[i] << "   | " << setprecision(7) << DF_xi << " | " << m[i] << " | " <<
			setprecision(18) << scientific<<abs(DF_xi - m[i]) << " | " << M5 / 60 * pow(h, 4) <<fixed<< endl;
	}

	// ������ ����������� ������������� �������
	fout << endl << " x   | " << "   f(x)   |" << "   S(f, x) |" << " �����������              |" << " ������ " << endl;
	for (int i = 0; i < n; i++)
	{
		double tau = (xMidVec[i] - xVec[i]) / h;
		double S = fi0(tau)*yVec[i] + fi0(1 - tau)*yVec[i + 1] +
			h*(fi1(tau)*m[i] - fi1(1 - tau)*m[i + 1]);

		double F_ = F(xMidVec[i]);

		fout << setprecision(1) << xMidVec[i] << "  | " << setprecision(7) << F_ << " | " <<
			S << " | " << setprecision(18) << scientific<< abs(S - F_) << " | " << FindErrorForSpline(M4, M5) <<fixed<< endl;
	}
}

// ������������ ������� 3�3
double Determinant(vector <vector<double>> A)
{
	return A[0][0] * A[1][1] * A[2][2] +
		A[0][1] * A[1][2] * A[2][0] +
		A[0][2] * A[1][0] * A[2][1] -
		A[0][2] * A[1][1] * A[2][0] -
		A[0][0] * A[2][1] * A[1][2] -
		A[2][2] * A[1][0] * A[0][1];
}

// ������ ������� ������� �� ������ ������
vector <vector<double>> ReplaceColumn(vector <vector<double>> A, int j, vector<double> b)
{
	for (int i = 0; i < 3; i++)
		A[i][j] = b[i];
	return A;
}

// ������� ���� ������� �������
vector<double> CramerMethod(vector <vector<double>> A, vector<double> b)
{
	double det = Determinant(A);

	return{ Determinant(ReplaceColumn(A, 0, b)) / det,
		Determinant(ReplaceColumn(A, 1, b)) / det,
		Determinant(ReplaceColumn(A, 2, b)) / det };
}

// �������, ���������� ������� ������������������� �����������
double FuncMeanSquare(double x, vector<double> c)
{
	return c[0] + c[1] * x + c[2] * x*x;
}

// ������������������ ����������� (���������� �������)
void MeanSquareApproximationForTable(vector<double> xVec, vector<double> yVec)
{
	fout << "���������� �������\n\n";

	vector <vector<double>> A(3);
	for (int i = 0; i < 3; i++)
	{
		A[i].resize(3);
		A[i] = { 0,0,0 };
	}

	for (int i = 0; i < n+1; i++)
	{
		A[0][0] ++;
		A[0][1] += xVec[i];
		A[0][2] += pow(xVec[i], 2);
		A[1][2] += pow(xVec[i], 3);
		A[2][2] += pow(xVec[i], 4);
	}
	A[1][0] = A[0][1];
	A[1][1] = A[0][2];
	A[2][0] = A[0][2];
	A[2][1] = A[1][2];

	fout << "�������:\n";
	PrintMatrix(A);

	vector<double > b = { 0,0,0 };

	for (int i = 0; i < n+1; i++)
	{
		b[0] += yVec[i];
		b[1] += yVec[i] * xVec[i];
		b[2] += yVec[i] * pow(xVec[i], 2);
	}

	fout << "\n������ ������ ������:\n";
	PrintVector(b);

	vector<double> c = CramerMethod(A, b);

	fout << setprecision(4) << "P2(x) = " << c[0] << " + (" << c[1] << "*x) + (" << c[2] << "*x^2)\n";

	double scalarFF = 0;
	double scalarFG = 0;

	for (int i = 0; i < n+1; i++)
	{
		scalarFF += pow(yVec[i], 2);
		scalarFG += pow(FuncMeanSquare(xVec[i], c), 2);
	}

	double error = sqrt(scalarFF - scalarFG);

	fout << "����� �����������: " <<setprecision(18) << error << endl;
}

// ������������������ ����������� (����������� �������)
void MeanSquareApproximationForInterval()
{
	fout << "\n����������� �������\n\n";

	vector <vector<double>> A = { {1    ,3 / 2.,7 / 3.},      //(1,1);(1,x);(1,x^2)
								{ 3 / 2.,7 / 3.,15 / 4.},     //(x,1);(x,x);(x,x^2)
								{ 7 / 3.,15 / 4.,31 / 5.} };  //(x^2,1);(x^2,x);(x^2,x^2)

	fout << "�������:\n";
	PrintMatrix(A);

	vector<double> b(3);
	
	if (var == 21)
		b = { 2/log(2)-5/2.,                                  //(f,1)
		(log(64)-2)/pow(log(2),2)-4,                             //(f,x)
		2*(2+7*pow(log(2),2)-3*log(4))/pow(log(2),3)-79/12.};   //(f,x^2)
	else
		b = { 6/log(3),
		1/6.+3*(log(243)-2)/ pow(log(3),2),
		1/2.+3*(4+11*pow(log(3),2)-5*log(9))/ pow(log(3),3) };

	fout << "\n������ ������ ������:\n";
	PrintVector(b);

	vector<double> c = CramerMethod(A, b);

	fout << setprecision(4) << "P2(x) = " << c[0] << " + (" << c[1] << "*x) + (" << c[2] << "*x^2)\n";


	double scalarFF = (var == 21) ? 7+(12-22*log(2))/pow(log(2), 2) : 1/3.+12*(log(243)-2)/pow(log(3),2);
	double scalarFG = (var == 21) ? 0.237112 : 35.0625;

	double error = sqrt(scalarFF - scalarFG);

	fout << "����� �����������: " << setprecision(6) << error << endl;
}

// ������� ��������� ������� �������� ������������
void ReverseInterpolation(vector<double> xVec, vector<double> yVec)
{
	fout << "\n�������� ������������\n\n";
	fout << "������� ����������� ���������:\n";
	vector <vector<double>> DivDiff = FindDividedDifferenceMatrix(yVec, xVec);
	PrintMatrixDivDiff(DivDiff);

	double x = DivDiff[0][1];
	double w = 1;
	double c = F((b-a)/2+a); // �������� ������� � �������� �������

	for (int i = 1; i < n+1; i++)
	{
		w *= c - yVec[i - 1];
		x += DivDiff[0][i + 1] * w;
	}

	fout << "\nF(x) = " << c << endl;
	fout << "\n������: " << x << endl;
	fout << "�������: " << setprecision(8) << abs(F(x) - c) << endl;
}

int main()
{
	vector <double> xVec(n+1);
	for (int i = 0; i < n+1; i++)
	{
		xVec[i] = a + i * h;
	}

	vector <double> xMidVec(n);
	for (int i = 0; i < n; i++)
	{
		xMidVec[i] = a + (i + 0.5) * h;
	}

	vector <double> yVec(n + 1);
	for (int i = 0; i < n+1; i++)
	{
		yVec[i] = F(xVec[i]);
	}

	NewtonFormula(xVec, xMidVec, yVec);
	SplineInterpolation(xVec, xMidVec,yVec);

	fout << "\n ������������������ �����������\n\n";
	MeanSquareApproximationForTable(xVec, yVec);
	MeanSquareApproximationForInterval();

	ReverseInterpolation(xVec, yVec);

	fout.close();
	return 0;
}
