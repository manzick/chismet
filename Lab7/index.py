from math import log
from math import sin

var = 23
eps1 = 0.00001   # для автоматического выбора шага
eps = 0.0001     # точность выполнения граничного условия


def space(n):
    ans = ''
    for _ in range(n):
        ans += ' '
    return ans


def a(x):
    if var == 23:
        return 30 * (x - 0.5)
    else:
        return 40 * (x + 1)


def b(x):
    if var == 23:
        return pow(x, 2) + 2
    else:
        return - pow(x, 2) + 2


def c(x):
    if var == 23:
        return 2 * x - 1
    else:
        return x + 1


def Ynp(x):
    return 1 + x + 10 * log(var + 1) * pow(x, 3) * pow(1 - x, 3)


def DYnp_DX(x):
    return 1 + 30 * log(var + 1) * pow((x - pow(x, 2)), 2) * (1 - 2 * x)


def D2Ynp_DX2(x):
    return 60 * log(var + 1) * (x - pow(x, 2)) * (pow(1 - 2 * x, 2) - x + pow(x, 2))


def F(x):
    return D2Ynp_DX2(x) + a(x) * DYnp_DX(x) - b(x) * Ynp(x) + c(x) * sin(Ynp(x))


def f(x, y, z):
    return -a(x) * z + b(x) * y - c(x) * sin(y) + F(x)


def find_h(h, x, y, z):
    """
    Функция для поиска шага h
    """
    k1 = h * z
    l1 = h * f(x, y, z)
    k2 = h * (z + l1 / 2)
    l2 = h * f(x + h / 2, y + k1 / 2, z + l1 / 2)
    k3 = h * (z + l2 / 2)
    l3 = h * f(x + h / 2, y + k2 / 2, z + l2 / 2)
    k4 = h * (z + l3)
    l4 = h * f(x + h, y + k3, z + l3)
    y = y + (k1 + 2 * k2 + 2 * k3 + k4) / 6
    z = z + (l1 + 2 * l2 + 2 * l3 + l4) / 6
    return y, z


def runge_kutta_method(alpha, xArg, print_table):
    x, y, z, y_prev = 0, 1, alpha, 0  # y(0)=1, y'(0)=alpha
    max_error = 0
    if print_table:
        print('     x     |   y(x)   |  Yпр(x) |  z(x)  | delta')
    while x < xArg:
        h = 0.1
        while True:
            h /= 2
            y_prev = y
            z_prev = z
            z1 = z
            y1, z1 = find_h(h, x, y_prev, z1)
            y2, z_prev = find_h(h / 2, x, y_prev, z_prev)
            y3, z_prev = find_h(h / 2, x, y2, z_prev)
            if abs(y1 - y3) <= eps1:
                break
        error = abs(Ynp(x) - y)
        if error > max_error:
            max_error = error
        if print_table:
            _x = str('{0.real:.5f}'.format(x))
            _y = str('{0.real:.5f}'.format(y))
            _Ynp = str('{0.real:.5f}'.format(Ynp(x)))
            _z = str('{0.real:.5f}'.format(z))
            print(
                space(3) + _x + space(3) + _y + space(3) + _Ynp + space(3) + _z + space(3) + str(error)
            )
        y_prev = y
        """
        k_ для уравнения y' = z
        l_ для уравнения z' = -Az + By - Сsin(y) + F
        """
        k1 = h * z
        l1 = h * f(x, y, z)
        k2 = h * (z + l1 / 2)
        l2 = h * f(x + h / 2, y + k1 / 2, z + l1 / 2)
        k3 = h * (z + l2 / 2)
        l3 = h * f(x + h / 2, y + k2 / 2, z + l2 / 2)
        k4 = h * (z + l3)
        l4 = h * f(x + h, y + k3, z + l3)
        y = y + (k1 + 2 * k2 + 2 * k3 + k4) / 6
        z = z + (l1 + 2 * l2 + 2 * l3 + l4) / 6
        x += h
    return y_prev, max_error


def main():
    alpha1, alpha2 = 0.0, 3.0
    b = 2.0
    itr = 0
    print('Метод стрельб:')
    print('  itr  |   z(0)   |    y(1)     |     delta')
    while True:
        itr += 1
        alpha = (alpha2 + alpha1) / 2
        y, delta = runge_kutta_method(alpha, 1, 0)
        if y > b:
            alpha2 = alpha
        else:
            alpha1 = alpha
        print(
            space(4) + str(itr) + space(4) + str('{0.real:.6f}'.format(alpha)) + space(4) + str('{0.real:.5f}'.format(y)) + space(5) + str(delta)
        )

        if delta <= eps:
            break
    y, delta = runge_kutta_method(alpha, 1, 1)
    print('Max delta: '+str(delta))


if __name__ == "__main__":
    main()
