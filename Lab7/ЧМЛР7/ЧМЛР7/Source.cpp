#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

const int variant = 23;
const double xi = 0.2;
const double Pi = 3.141592653589793;
ofstream fout("out.txt");


double u(double t, double x)
{
	return x + 0.1 * t * sin(Pi*x) * variant;
}

double f(double t, double x)
{
	return 0.1 * variant * sin(Pi * x) + 0.1 * xi * Pi * Pi * t * variant * sin(Pi * x);
}

double find_delta(vector<double> x_vec, double i, int yav_neyav)
{
	int n = x_vec.size() - 1;
	double h = 1. / n;
	double tau = (yav_neyav == 1) ? h * h / (4 * xi) : h;
	double delta = 0;
	for (int j = 0; j <= n; j++)
	{
		double del = abs(u(tau*i, j*h) - x_vec[j]);
		if (del > delta)
			delta = del;
	}
	return delta;
}

vector <double> progonka(vector <double> xVec,int n,double h,double tau,int i)
{
	double d = (tau * xi) / (h * h);
	vector <double> xVecNew(n + 1);
	xVecNew[0] = xVec[0];
	xVecNew[n] = xVec[n];
	vector <double> alpha(n + 1);
	vector <double> beta(n + 1);

	alpha[1] = 0;
	beta[1] = xVecNew[0];

	for (int j = 1; j < n; j++)
	{
		alpha[j + 1] = d / (-d*alpha[j] + 1 + 2 * d);
		beta[j + 1] = (xVec[j] + tau*f(tau*i, j*h) + d*beta[j]) / (1 + 2 * d - d*alpha[j]);
	}

	for (int j = n - 1; j > 0; j--)
		xVecNew[j] = alpha[j + 1] * xVecNew[j + 1] + beta[j + 1];
	return xVecNew;
}


void neyavn_shema(int n)
{
		double max_delta = 0, delta;
		vector <double> xVec(n+1);
		double h = 1. / n, tau = h;

		fout << "N=" << n << endl << setw(7) << "t|" << setw(22)
			<< "delta     | x:";
		for (int i = 0; i < n + 1; i++)
		{
			xVec[i] = h*i;
			fout << setw(12) << xVec[i];
		}
		fout << endl;

		for (int i = 1; tau * i <= 1.; i++)
		{
			xVec = progonka(xVec,n,h,tau,i);
			delta = find_delta(xVec, i, 0);

			fout << setw(7) << setprecision(3) << fixed << tau * i << setw(22) << setprecision(18);
			if (delta < 0.0001)
				fout << setprecision(14) << scientific << delta;
			else
				fout << fixed << delta;

			for (int j = 0; j < xVec.size(); j++)
				fout << setw(12) << setprecision(8) << fixed << xVec[j];

			fout << endl;
			if (delta > max_delta)
				max_delta = delta;
		}
		fout << "Del_T=" << max_delta << endl;
}

void yavn_shema(int n)
{
		double max_delta=0, delta;
		vector <double> xVec(n+1),xVecNew(n+1);
		double h = 1.0 / n, tau = h * h / (4 * xi);

		fout << "N=" << n << endl << setw(7) << "t|" << setw(22)
			<< "delta     | x:";
		for (int i = 0; i < n + 1; i++)
		{
			xVec[i] = h*i;
			fout << setw(12) << xVec[i];
		}
		fout << endl;

		for (int i = 1; tau * i <= 1.; i++)
		{
			double t = tau * i;

			xVecNew[0] = xVec[0];
			xVecNew[n] = xVec[n];

			for (int j = 1; j < n; j++)
				xVecNew[j] = xVec[j] + tau * (xi *
					(xVec[j + 1] - 2 * xVec[j] + xVec[j - 1]) / (h * h) + f(t-tau, j*h));
			xVec = xVecNew;

			delta = find_delta(xVec, i, 1);

			fout << setw(7) << setprecision(3) << fixed << t << setw(22) << setprecision(18);
			if (delta < 0.0001)
				fout << setprecision(14) << scientific << delta;
			else
				fout << fixed << delta;

			for (int j = 0; j < xVec.size(); j++)
				fout << setw(12) << setprecision(8) << fixed << xVec[j];

			fout << endl;
			if (delta > max_delta)
				max_delta = delta;
		}
		fout << "Del_T=" << max_delta << endl;
}

int main()
{
	fout <<"xi= "<< xi<< endl;
	fout << "����� �������� ��������� (����� �����)\n";
	yavn_shema(8);
	yavn_shema(16);
	yavn_shema(32);
	fout << "����� �������� ��������� (������� �����)\n";
	neyavn_shema(8);
	neyavn_shema(16);
	neyavn_shema(32);
	fout.close();
	return 0;
}
