from cmath import log10
from math import sqrt

var = 23


def space(n):
    """
    Генерирует n пробелов, для красивого вывода
    """
    ans = ''
    for _ in range(n):
        ans += ' '
    return ans


def print_line(N, h, summary, err, k):
    """
    Длинная строка 'красивого' вывода
    """
    _h = str('{0.real:.4f}'.format(h))
    _summary = str('{0.real:.6f}'.format(summary))
    _err = str('{0.real:.13f}'.format(err))
    _k = str('{0.real:.4f}'.format(k))
    print(
        space(10) + str(N) + '|' + space(9) + _h + '|' + space(12)
        + _summary + '|' + space(23) + _err + '|' + space(9) + _k + '|'
    )


def f(x):
    """
    Исходная функция
    """
    if var == 23:
        return pow(3, x) + 2 * x - 3
    else:
        return pow(2, x) - 3 * x + 2


def df(x):
    """
    Производная исходной функции
    """
    if var == 23:
        return log10(3)*pow(3, x) + 2
    else:
        return log10(2)*pow(2, x) - 3


def part_integral(N, a, b, h, meth, c):
    """
    Вычисление интеграла на определенном шаге
    meth = 1 формула трапеций
    meth = 2 модифицированная формула трапеций
    meth = 3 формула Симпсона
    meth = 4 формула Гаусса
    """
    sum = 0.0
    if meth == 1:
        for i in range(N):
            _a = a + i * h
            _b = a + (i + 1) * h
            sum += (_b - _a) * (f(_b) + f(_a)) / 2
        c += N / 2
    elif meth == 2:
        for i in range(1, N):
            sum += f(a + i * h)
        sum = h * ((f(a) + f(b)) / 2 + sum) + (h * h) / 12 * (df(a) - df(b))
        c += N / 2
    elif meth == 3:
        for i in range(N):
            _a = a + i * h
            _b = a + (i + 1) * h
            sum += ((_b - _a) / 6) * (f(_a) + 4 * f((_a + _b) / 2) + f(_b))
        c += N
    elif meth == 4:
        k, a02, a1 = 0.0, 5 / 9, 8 / 9
        for i in range(N):
            k = a + (2 * i + 1) * h / 2
            sum += h / 2 * (a02 * f(k - h * sqrt(0.6) / 2) + a1 * f(k) + a02 * f(k + h * sqrt(0.6) / 2))
            c += 3
    return sum, c


def count_integral(a, b, meth):
    """
    Вычисление интеграла
    """
    print(space(10) + 'N' + space(8) + 'h' + space(15) + 'Интеграл' +
          space(20) + 'Оценка погрешности' + space(10) + 'k')
    N, count = 1, 0
    h, summary,  term, err, k = 1.0, 0.0, 0.0, 1.0, 0.0
    meth_list = [1 / 3, 1 / 3, 1 / 3, 1 / 15, 1 / 63]
    tet = meth_list[meth]
    s0, count = part_integral(N, a, b, h, meth, count)
    s1, count = part_integral(N * 2, a, b, h, meth, count)
    while abs(err) > 10E-8:
        term = summary
        h = (b - a) / N
        summary, count = part_integral(N, a, b, h, meth, count)
        err = (summary - term) * tet
        mul_s = s1 - s0
        if mul_s == 0.0:
            mul_s = 1
        k = log10((summary - s0) / mul_s - 1) / log10(0.5)
        s0 = s1
        s1 = summary
        print_line(N, h, summary, err, k)
        N *= 2
    if meth != 4:
        count += 2
    print('Результат: ' + str('{0.real:.6f}'.format(summary)))
    print('Количество обращений: ' + str(count))


def main():
    a, b = 1.0, 2.0
    print('Вариант ' + str(var) + ':')
    print('Метод трапеций:')
    count_integral(a, b, 1)
    print('Метод  трапеций модифицированный:')
    count_integral(a, b, 2)
    print('Метод  Симпсона:')
    count_integral(a, b, 3)
    print('Метод  Гаусса:')
    count_integral(a, b, 4)


if __name__ == "__main__":
    main()
