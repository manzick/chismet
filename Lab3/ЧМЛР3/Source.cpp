#include "alglib/stdafx.h"
#include <fstream>
#include <iomanip>
#include <math.h>
#include "alglib/linalg.h"
using namespace std;
using namespace alglib;

const int n = 4;
const double eps = 0.0001;
ofstream fout("out.txt");

// ����������� �������� ������ ������� � ������
void CopyVector(double* a, double* b)
{
	for (int i = 0; i < n; i++)
		b[i] = a[i];
}

// �������� ��������
void SubVector(double* a, double* b, double* c)
{
	for (int i = 0; i < n; i++)
		c[i] = a[i] - b[i];
}


// ������ ���������� �������
void PrintHeadings()
{
	fout << setw(41) << "�����     |  " << "������" << endl;
	fout << " Itr   |  tau   |  q     |  �������   |����������� |  x[1]    x[2]     x[3]    x[4]" << endl;
}

// ������ ������ �� ������� ��������
void PrintData(int itr, double tau, double q, double r, double error, double* x)
{
	fout << " " << setw(4) << itr << "  | " << setprecision(4) << tau << " | " << fixed << setprecision(4) << q <<
		" | " << setprecision(8) << r << " | " << error << " | " << setprecision(5) <<
		x[0] << "  " << x[1] << "  " << x[2] << "  " << x[3] << endl;
}

// ������ �������
void PrintMatrix(double** A)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			fout << fixed << setprecision(1) << setw(7) << A[i][j];
		fout << endl;
	}
	fout << "\n";
}

// ������ �������
void PrintVector(double* b)
{
	for (int i = 0; i < n; i++)
		fout << b[i] << endl;
	fout << "\n";
}

// ��������� ������� �� ������
void MatrixMultipleVector(double** A, double* b, double* v)
{
	for (int i = 0; i < n; i++)
	{
		v[i] = 0;
		for (int k = 0; k < n; k++)
			v[i] += A[i][k] * b[k];
	}
}

// ����� �������
double VectorNorm1(double* b)
{
	double max = 0;
	for (int i = 0; i < n; i++)
		if (abs(b[i]) > max)
			max = abs(b[i]);
	return max;
}

// ����� �������
double Norm1(double** A)
{
	double max = 0;

	for (int i = 0; i < n; i++)
	{
		double sum = 0;
		for (int j = 0; j < n; j++)
			sum += abs(A[i][j]);
		if (sum > max)
			max = sum;
	}

	return max;
}

// ��������� ������������ ��������
double ScalarMultiple(double* a, double* b)
{
	double sum = 0;
	for (int i = 0; i < n; i++)
		sum += a[i] * b[i];
	return sum;
}

// ����������� ������ ��-��� �������
void DeleteMatrix(double** A)
{
	for (int i = 0; i < n; i++)
		delete[] A[i];
	delete[] A;
}

// ���������� �������� �������
void ReverseMatrix(double** A, double** Ar)
{
	real_2d_array a;
	a.setlength(n, n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			a[i][j] = A[i][j];

	ae_int_t info;
	matinvreport rep;
	rmatrixinverse(a, info, rep);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			Ar[i][j] = a[i][j];
}

// ����� ������� ��������
void SimpleIterMethod(double** A, double* x, double* b)
{
	fout << "����� ������� ��������" << endl << endl;
	PrintHeadings();

	double tau = 1.8 / Norm1(A);
	int itr = 0;
	double* Ax = new double[n];
	double* r = new double[n]; // �������
	double rNorm; // ����� �������
	double* xNew = new double[n]; // x(k+1)
	double* xPrev = new double[n]; // x(k-1)
	double* temp1 = new double[n];
	double* temp2 = new double[n];

	do
	{
		itr++;

		for (int i = 0; i < n; i++)
		{
			double sum = 0;
			for (int j = 0; j < n; j++)
				sum += A[i][j] * x[j];
			xNew[i] = x[i] + tau * (b[i] - sum);
		}

		SubVector(xNew, x, temp1);
		SubVector(x, xPrev, temp2);

		double q = (itr == 1) ? VectorNorm1(xNew) : VectorNorm1(temp1) / VectorNorm1(temp2);
		double error = VectorNorm1(temp1)*(1 - q) / q;

		MatrixMultipleVector(A, x, Ax);
		SubVector(Ax, b, r);

		rNorm = VectorNorm1(r) / VectorNorm1(xNew);

		CopyVector(x, xPrev);
		CopyVector(xNew, x);

		PrintData(itr, tau, q, rNorm, error, x);

	} while (rNorm > eps);

	delete[] xNew;
	delete[] xPrev;
	delete[] r;
	delete[] Ax;
	delete[] temp1;
	delete[] temp2;
}

// ����� ���������� ������
void SteepestDescentMethod(double** A, double* x, double* b)
{
	fout << "����� ���������� ������" << endl << endl;
	PrintHeadings();

	double tau;
	int itr = 0;
	double* Ax = new double[n];
	double* r = new double[n]; // �������
	double rNorm; // ����� �������
	double* xNew = new double[n]; // x(k+1)
	double* xPrev = new double[n]; // x(k-1)
	double* temp1 = new double[n];
	double* temp2 = new double[n];
	double* Ar = new double[n];

	do
	{
		itr++;

		MatrixMultipleVector(A, x, Ax);
		SubVector(Ax, b, r);

		MatrixMultipleVector(A, r, Ar);
		tau = ScalarMultiple(r, r) / ScalarMultiple(Ar, r);

		for (int i = 0; i < n; i++)
		{
			double sum = 0;
			for (int j = 0; j < n; j++)
				sum += A[i][j] * x[j];
			xNew[i] = x[i] + tau * (b[i] - sum);
		}

		SubVector(xNew, x, temp1);
		SubVector(x, xPrev, temp2);

		double q = (itr == 1) ? VectorNorm1(xNew) : VectorNorm1(temp1) / VectorNorm1(temp2);
		double error = VectorNorm1(temp1)*(1 - q) / q;

		rNorm = VectorNorm1(r) / VectorNorm1(xNew);

		CopyVector(x, xPrev);
		CopyVector(xNew, x);

		PrintData(itr, tau, q, rNorm, error, x);

	} while (rNorm > eps);

	delete[] xNew;
	delete[] xPrev;
	delete[] r;
	delete[] Ax;
	delete[] temp1;
	delete[] temp2;
	delete[] Ar;
}

// ����� ������������ �������� w ��� ������ ���
double FindOptimalW(double** A, double* x, double* b)
{
	int itr = 0;
	double* Ax = new double[n];
	double* xTemp = new double[n];
	double* r = new double[n]; // �������
	double rNorm; // ����� �������
	double* xNew = new double[n]; // x(k+1)	
	double wOpt = 0;
	int itrMin = INT_MAX;
	CopyVector(x,xTemp);

	fout << "����� ������������ �������� W ��� ������ ���" << endl << endl;

	double w = 0.1;
	while (w < 2)
	{
		itr = 0;

		do
		{
			itr++;
			for (int i = 0; i < n; i++)
			{
				double s1 = 0;
				double s2 = 0;

				for (int j = 0; j <= i - 1; j++)
					s1 += A[i][j] * xNew[j];
				for (int j = i + 1; j < n; j++)
					s2 += A[i][j] * x[j];

				double x_ = 1 / A[i][i] * (b[i] - s1 - s2);
				xNew[i] = x[i] + w * (x_ - x[i]);
			}

			MatrixMultipleVector(A, x, Ax);
			SubVector(Ax, b, r);

			rNorm = VectorNorm1(r) / VectorNorm1(xNew);

			CopyVector(xNew, x);

		} while (rNorm > 0.01);

		if (itr < itrMin)
		{
			itrMin = itr;
			wOpt = w;
		}

		fout << "w = " << setprecision(1) << w << ", itr = " << itr << endl;
		w += 0.1;
		CopyVector(xTemp,x);
	}

	fout << "\nwOpt = " << wOpt << ", itrMin = " << itrMin << endl << endl;

	delete[] xNew;
	delete[] r;
	delete[] Ax;
	delete[] xTemp;

	return wOpt;
}

// ����� ���
void SOR(double** A, double* x, double* b, double wOpt)
{
	int itr = 0;
	double* Ax = new double[n];
	double* r = new double[n]; // �������
	double rNorm; // ����� �������
	double* xNew = new double[n]; // x(k+1)
	double* xPrev = new double[n]; // x(k-1)
	double* temp1 = new double[n];
	double* temp2 = new double[n];


	fout << "����� ���" << endl << endl;
	PrintHeadings();

	do
	{
		itr++;

		for (int i = 0; i < n; i++)
		{
			double s1 = 0;
			double s2 = 0;

			for (int j = 0; j <= i - 1; j++)
				s1 += A[i][j] * xNew[j];
			for (int j = i + 1; j < n; j++)
				s2 += A[i][j] * x[j];

			double x_ = 1 / A[i][i] * (b[i] - s1 - s2);
			xNew[i] = x[i] + wOpt * (x_ - x[i]);
		}

		SubVector(xNew, x, temp1);
		SubVector(x, xPrev, temp2);

		double q = (itr == 1) ? VectorNorm1(xNew) : VectorNorm1(temp1) / VectorNorm1(temp2);
		double error = VectorNorm1(temp1)*(1 - q) / q;

		MatrixMultipleVector(A, x, Ax);
		SubVector(Ax, b, r);

		rNorm = VectorNorm1(r) / VectorNorm1(xNew);

		CopyVector(x, xPrev);
		CopyVector(xNew, x);

		PrintData(itr, wOpt, q, rNorm, error, x);

	} while (rNorm > eps);


	delete[] xNew;
	delete[] xPrev;
	delete[] r;
	delete[] Ax;
	delete[] temp1;
	delete[] temp2;
}

// ����� ����������� ����������
void ConjugateGradientMethod(double** A, double* x, double* b)
{
	fout << "����� ����������� ����������" << endl << endl;
	PrintHeadings();

	int itr = 0;
	double* Ax = new double[n];
	double* r = new double[n]; // �������
	double rNorm; // ����� �������
	double* xNew = new double[n]; // x(k+1)
	double* xPrev = new double[n]; // x(k-1)
	double* temp1 = new double[n];
	double* temp2 = new double[n];
	double* Ar = new double[n];
	double tau, tauNew;
	double scalarMultPrev, scalarMultCur;
	double alpha;

	do
	{
		itr++;

		MatrixMultipleVector(A, x, Ax);
		SubVector(Ax, b, r);

		MatrixMultipleVector(A, r, Ar);
		scalarMultCur = ScalarMultiple(r, r);
		tauNew = scalarMultCur / ScalarMultiple(Ar, r);

		alpha = (itr == 1) ? 1 :
			1 / (1 - tauNew * scalarMultCur / (tau*alpha*scalarMultPrev));

		scalarMultPrev = scalarMultCur;
		tau = tauNew;


		for (int i = 0; i < n; i++)
		{
			xNew[i] = alpha*x[i] + (1 - alpha)*xPrev[i] - tauNew*alpha*r[i];
		}

		SubVector(xNew, x, temp1);
		SubVector(x, xPrev, temp2);

		double q = (itr == 1) ? VectorNorm1(xNew) : VectorNorm1(temp1) / VectorNorm1(temp2);
		double error = VectorNorm1(temp1)*(1 - q) / q;

		rNorm = VectorNorm1(r) / VectorNorm1(xNew);

		CopyVector(x, xPrev);
		CopyVector(xNew, x);

		PrintData(itr, tauNew, q, rNorm, error, x);

	} while (rNorm > eps);

	delete[] xNew;
	delete[] xPrev;
	delete[] r;
	delete[] Ax;
	delete[] temp1;
	delete[] temp2;
	delete[] Ar;
}


int main()
{
	setlocale(LC_ALL, "Russian");
	ifstream fi("in.txt");

	double **A = new double *[n];
	for (int i = 0; i < n; i++)
		A[i] = new double[n];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			fi >> A[i][j];

	fout << "A:\n";
	PrintMatrix(A);

	double* b = new double[n];
	for (int i = 0; i < n; i++)
		b[i] = i + 1;

	fout << "b:\n";
	PrintVector(b);

	double *x = new double[n];
	int timeStart, timeFinish;
	double timeSIM, timeSDM, timeSOR, timeCGM;
	
	CopyVector(b, x);
	timeStart = clock();
	SimpleIterMethod(A, x, b);
	timeFinish = clock();
	timeSIM = (timeFinish - timeStart) / 1000.;
	fout << endl;

	CopyVector(b, x);
	timeStart = clock();
	SteepestDescentMethod(A, x, b);
	timeFinish = clock();
	timeSDM = (timeFinish - timeStart) / 1000.;
	fout << endl;

	CopyVector(b, x);
	double wOpt = FindOptimalW(A, x, b);
	
	CopyVector(b, x);
	timeStart = clock();
	SOR(A, x, b, wOpt);
	timeFinish = clock();
	timeSOR = (timeFinish - timeStart) / 1000.;
	fout << endl;

	CopyVector(b, x);
	timeStart = clock();
	ConjugateGradientMethod(A, x, b);
	timeFinish = clock();
	timeCGM = (timeFinish - timeStart) / 1000.;
	fout << endl << endl;

	double **Ar = new double *[n];
	for (int i = 0; i < n; i++)
		Ar[i] = new double[n];

	ReverseMatrix(A, Ar);

	double cond = Norm1(A)*Norm1(Ar);
	fout << "����� ��������������� � = " << cond << endl << endl;

	fout << "������������� ������ ����� ��������: \n";
	double itr1 = log(1 / eps) / 2 * cond;
	fout << "� ������ ������� �������� � ������������ ������: " << alglib::round(itr1) << endl;
	double itr3 = log(1 / eps) / 4 * sqrt(cond);
	fout << "� ������ ���: " << alglib::round(itr3) << endl;
	double itr4 = log(2 / eps) / 2 * sqrt(cond);
	fout << "� ������ ����������� ����������: " << alglib::round(itr4) << endl << endl;

	fout << "��������� �������" << endl;
	fout << "����� ������� ��������: " << setprecision(3) << timeSIM << " c" << endl;
	fout << "����� ������������ ������: " << setprecision(3) << timeSDM << " c" << endl;
	fout << "����� ���: " << setprecision(3) << timeSOR << " c" << endl;
	fout << "����� ����������� ����������: " << setprecision(3) << timeCGM << " c" << endl;

	DeleteMatrix(A);
	DeleteMatrix(Ar);
	delete[] b;
	delete[] x;

	fi.close();
	fout.close();
	return 0;
}
