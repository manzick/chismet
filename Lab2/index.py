import numpy

n = 4
var = '23.txt'


def get_data(name):
    """
    Считывание исходных данных из файла
    :param name: имя файла
    :return: двумерный массив
    """
    with open(name, 'r') as file:
        data = file.readlines()
    data = [[float(item) for item in temp.split()] for temp in data]
    return data


def matrix_mult_vect(matrix, vect):
    """
    Функция умножения матрицы на вектор
    :param matrix: матрица
    :param vect: вектор
    :return:
    """
    answer = []
    for line in matrix:
        mega_temp = 0
        for one, two in zip(line, vect):
            temp = one * two
            mega_temp += temp
        answer.append(mega_temp)
    return answer


def print_round(matrix):
    """
    Округляет все элементы списка до 7 знаков после запятой
    """
    if isinstance(matrix, float):
        print('%.7f' % matrix)
        return matrix
    if isinstance(matrix[0], list):
        for mini in matrix:
            print(['%.7f' % elem for elem in mini])
        return matrix
    else:
        print(['%.7f' % elem for elem in matrix])


def swap_rows(matrix, one, two):
    """
    Меняет местами столбцы в матрицах
    :param matrix: матрица
    :param one: первый столбец
    :param two: второй столбец
    :return:
    """
    for k in range(0, n):
        matrix[one][k], matrix[two][k] = matrix[two][k], matrix[one][k]
    return matrix


def get_strange_matrix():
    """
    Выдает матрицу у которой на главной диагонали 1, а все остальное нули
    """
    matrix = []
    for i in range(n):
        matrix.append([])
        for j in range(n):
            matrix[i].append((1 if i == j else 0))
    return matrix


def transpose_matrix(matrix):
    """
    Функция транспонирования матрицы
    """
    new_matrix = [[i for i in elem] for elem in matrix]
    for k in range(0, n - 1):
        for i in range(k + 1, n):
            new_matrix[k][i], new_matrix[i][k] = new_matrix[i][k], new_matrix[k][i]
    return new_matrix


def reverse_matrix(Lr, U, P):
    matrix = get_data(var)
    for k in range(0, n):
        e = []
        for i in range(0, n):
            e.append(0)
        e[k] = 1
        x = find_x(Lr, U, P, e, [0.0, 0.0, 0.0, 0.0])
        for i in range(0, n):
            matrix[i][k] = x[i]
    return matrix


def multiply_matrix(a, b):
    """
    Функция перемножения матрицы
    :param a: первая матрица
    :param b: вторая матрица
    :return: результат
    """
    c = get_strange_matrix()
    for i in range(0, n):
        for j in range(0, n):
            c[i][j] = 0
            for k in range(0, n):
                c[i][j] += a[i][k] * b[k][j]
    return c


def multiply_matrix_vector(matrix, vector):
    """
    Умножения матрицы на вектор
    :return:
    """
    answer = []
    for i in range(0, n):
        answer.append(0.0)
        for k in range(0, n):
            answer[i] += matrix[i][k] * vector[k]
    return answer


def subtraction_matrix(a, b):
    for i in range(n):
        for j in range(n):
            a[i][j] -= b[i][j]
    return a


def determinant(Lr, count_swaps):
    det = 1.0
    for i in range(0, n):
        det /= Lr[i][i]
    det *= (1 if count_swaps % 2 == 0 else -1)
    return det


def norm_1(matrix):
    max = 0
    for i in range(0, n):
        sum = 0
        for j in range(0, n):
            sum += abs(matrix[i][j])
        if sum > max:
            max = sum
    return max


def norm_2(matrix):
    max = 0
    for j in range(0, n):
        sum = 0
        for i in range(0, n):
            sum += abs(matrix[i][j])
        if sum > max:
            max = sum
    return max


def norm_3(matrix):
    matrix_t = transpose_matrix(matrix)
    a = multiply_matrix(matrix_t, matrix)
    one, _ = numpy.linalg.eig(a)
    ans = max(one)
    return numpy.sqrt(ans)


def print_lu():
    """
    Функция-цикл для печати LU итераций
    """
    count_swaps = 0
    L0 = get_strange_matrix()
    P = get_strange_matrix()
    r = n

    U = get_data(var)
    for k in range(0, n):
        max_elem = abs(U[k][k])
        ind_max_elem = k

        for i in range(k + 1, n):
            if abs(U[i][k]) > max_elem:
                max_elem = abs(U[i][k])
                ind_max_elem = i

        if k != ind_max_elem:
            U = swap_rows(U, k, ind_max_elem)
            L0 = swap_rows(L0, k, ind_max_elem)
            P = swap_rows(P, k, ind_max_elem)
            count_swaps += 1
        print()
        print('k = ' + str(k + 1) + ', m = ' + str(ind_max_elem + 1))
        if abs(max_elem) < 0.000001:
            r = k

        if r != n:
            print('rang = ' + str(r))
            exit()
        temp = U[k][k]
        for j in range(0, n):
            U[k][j] /= temp
            L0[k][j] /= temp

        for i in range(k + 1, n):
            tmp = U[i][k]
            for j in range(k, n):
                U[i][j] = U[i][j] - U[k][j] * tmp
            for j in range(0, n):
                L0[i][j] = L0[i][j] - L0[k][j] * tmp

        print('U:')
        print_round(U)
        Pr = transpose_matrix(P)
        Lr = multiply_matrix(L0, Pr)
        print('L:')
        print_round(Lr)
    return Lr, U, P, count_swaps


def find_x(Lr, U, P, b, x):
    temp = multiply_matrix_vector(P, b)
    y = multiply_matrix_vector(Lr, temp)
    x[n - 1] = y[n - 1]
    for i in range(n - 2, -1, -1):
        s = 0.0
        for j in range(i + 1, n):
            s += U[i][j] * x[j]
        x[i] = y[i] - s
    return x


def main():
    A = get_data(var)
    x = [1.0, 2.0, 3.0, 4.0]
    print('x:')
    print_round(x)
    print('A:')
    print_round(A)
    b = matrix_mult_vect(A, x)
    print('b:')
    print_round(b)
    Lr, U, P, count_swaps = print_lu()
    print()
    print('P:')
    print_round(P)
    x = find_x(Lr, U, P, b, x)
    print()
    print('x:')
    print_round(x)
    Ar = reverse_matrix(Lr, U, P)
    print()
    print('A^(-1):')
    print_round(Ar)
    print()
    print('Определитель А: ', end='')
    print_round(determinant(Lr, count_swaps))
    print('cond(A)1 =', norm_1(A)*norm_1(Ar))
    print('cond(A)2 =', norm_2(A)*norm_2(Ar))
    print('cond(A)3 =', norm_3(A)*norm_3(Ar))


if __name__ == "__main__":
    main()
